import 'jquery';


$(document).ready(function () {

    $('.b-burger').on('click', function() {
        const menu = $('.b-menu');

        $(this).toggleClass('active');
        menu.toggleClass('active');
    });
});

$(window).on('load resize orientationchange scroll', function () {
    if ( $(this).width() > 991 ) {
        $('.b-burger').removeClass('active');
        $('.b-menu').removeClass('active');
    }
});


















